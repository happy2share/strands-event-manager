package com.strands.interviews.eventsystem;


public interface Task2EventListener {
	
	void handleTask2Event(InterviewEvent event);

    Class[] getHandledEventClasses();
}
