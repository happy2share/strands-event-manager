package com.strands.interviews.eventsystem;

public class Task2EventListenerMock implements Task2EventListener {

	Class[] classes;
	
	public Task2EventListenerMock(Class[] classes) {
	        this.classes = classes;
	}

	@Override
	public void handleTask2Event(InterviewEvent event) {
		// TODO Auto-generated method stub
		System.out.println("Task2 Event called");
	}

	@Override
	public Class[] getHandledEventClasses() {
		// TODO Auto-generated method stub
		return classes;
	}

}
