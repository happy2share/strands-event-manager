package com.strands.interviews.eventsystem.impl;

import com.strands.interviews.eventsystem.EventManager;
import com.strands.interviews.eventsystem.InterviewEvent;
import com.strands.interviews.eventsystem.InterviewEventListener;
import com.strands.interviews.eventsystem.Task2EventListener;
import com.strands.interviews.eventsystem.Task2EventListenerMock;
import com.strands.interviews.eventsystem.events.CreationEvent;
import com.strands.interviews.eventsystem.events.SimpleEvent;
import com.strands.interviews.eventsystem.events.SubEvent;

import java.util.*;

/**
 * Manages the firing and receiving of events.
 *
 * <p>Any event passed to {@link #publishEvent} will be passed through to "interested" listeners.
 *
 * <p>Event listeners can register to receive events via
 * {@link #registerListener(String, com.strands.interviews.eventsystem.InterviewEventListener)}
 */
public class DefaultEventManager implements EventManager
{
    private Map listeners = new HashMap();
    private Map listenersByClass = new HashMap();
    public int eventsCount = 0;

    public void publishEvent(InterviewEvent event, String listenerName)
    {
        if (event == null)
        {
            System.err.println("Null event fired?");
            return;
        }
        sendEventTo(event, calculateListeners(event.getClass()), listenerName);
    }
    
    private Collection calculateListeners(Class eventClass)
    {
        return (Collection) listenersByClass.get(eventClass);
    }

    public void registerListener(String listenerKey, InterviewEventListener listener)
    {
        if (listenerKey == null || listenerKey.equals(""))
            throw new IllegalArgumentException("Key for the listener must not be null: " + listenerKey);

        if (listener == null)
            throw new IllegalArgumentException("The listener must not be null: " + listener);

        if (listeners.containsKey(listenerKey))
            unregisterListener(listenerKey);

        Class[] classes = listener.getHandledEventClasses();
        
        for (int i = 0; i < classes.length; i++)
            addToListenerList(classes[i], listener);

        listeners.put(listenerKey, listener);
    }
    public void registerTask2Listener(String listenerKey, Task2EventListener listener)
    {
        if (listenerKey == null || listenerKey.equals(""))
            throw new IllegalArgumentException("Key for the listener must not be null: " + listenerKey);

        if (listener == null)
            throw new IllegalArgumentException("The listener must not be null: " + listener);

        if (listeners.containsKey(listenerKey))
            unregisterTask2Listener(listenerKey);

        Class[] classes = listener.getHandledEventClasses();
        
        int classesLen = classes.length;
        
        if(classesLen == 0){
        	String[] listenersKey = {"createEvent.key", "simpleEvent.key", "subEvent.key"};
        	int listenersKeyArrLen = listenersKey.length;
        	Task2EventListenerMock eventListenerMock;
        	for (int i = 0; i < listenersKeyArrLen; i++){      
                switch(i){
                case 0:
                	eventListenerMock = new Task2EventListenerMock(new Class[]{CreationEvent.class});
            		registerTask2Listener(listenersKey[0], eventListenerMock);
                	publishEvent(new CreationEvent(this), "task2");
                	eventsCount++;
                	break;
                case 1:
                	eventListenerMock = new Task2EventListenerMock(new Class[]{SimpleEvent.class});
            		registerTask2Listener(listenersKey[1], eventListenerMock);
                	publishEvent(new SimpleEvent(this), "task2");
                	eventsCount++;
                	break;
                case 2:
                	eventListenerMock = new Task2EventListenerMock(new Class[]{SubEvent.class});
            		registerTask2Listener(listenersKey[2], eventListenerMock);
                	publishEvent(new SubEvent(this), "task2");
                	eventsCount++;
                	break;
                }   
                isTask2Done();
        	}
        }
        else{
        	for (int i = 0; i < classes.length; i++)
                addToListenerList(classes[i], listener);
            listeners.put(listenerKey, listener);
        }        
    }
   public boolean isTask2Done(){
	   if(eventsCount == 3){
   		return true;
   	}
	   return false;
   }
    public void unregisterListener(String listenerKey)
    {
        InterviewEventListener listener = (InterviewEventListener) listeners.get(listenerKey);

        for (Iterator it = listenersByClass.values().iterator(); it.hasNext();)
        {
            List list = (List) it.next();
            list.remove(listener);
        }

        listeners.remove(listenerKey);
    }
    
    public void unregisterTask2Listener(String listenerKey)
    {
        Task2EventListener listener = (Task2EventListener) listeners.get(listenerKey);

        for (Iterator it = listenersByClass.values().iterator(); it.hasNext();)
        {
            List list = (List) it.next();
            list.remove(listener);
        }

        listeners.remove(listenerKey);
    }
    
    private void sendEventTo(InterviewEvent event, Collection listeners, String listenerName)
    {
        if (listeners == null || listeners.size() == 0)
            return;
        for (Iterator it = listeners.iterator(); it.hasNext();)
        {
        	if(listenerName == "interview"){
        		InterviewEventListener eventListener = (InterviewEventListener) it.next();
                eventListener.handleEvent(event);
        	}else if(listenerName == "task2"){
        		Task2EventListener eventListener = (Task2EventListener) it.next();
                eventListener.handleTask2Event(event);
        	}
            
        }
    }

    private void addToListenerList(Class aClass, Object listener)
    {
        if (!listenersByClass.containsKey(aClass))
            listenersByClass.put(aClass, new ArrayList());

        ((List)listenersByClass.get(aClass)).add(listener);
    }

	public Map getListeners()
    {
    	System.out.println(listeners);
        return listeners;
    }
}
