package com.strands.interviews.eventsystem;

import static org.junit.Assert.*;

import org.junit.Test;

import com.strands.interviews.eventsystem.events.SimpleEvent;
import com.strands.interviews.eventsystem.impl.DefaultEventManager;

public class Task2EventListenerTest {

	private EventManager eventManager = new DefaultEventManager();
	@Test
	public void testTask2EventListening() {
		Task2EventListenerMock eventListenerMock = new Task2EventListenerMock(new Class[] {});
        eventManager.registerTask2Listener("task2.successKey", eventListenerMock); 
        assertTrue(eventManager.isTask2Done());
	}
	@Test
	public void testTask2EventListenFailing() {
		Task2EventListenerMock eventListenerMock = new Task2EventListenerMock(new Class[]{SimpleEvent.class});
        eventManager.registerTask2Listener("task2.failureKey", eventListenerMock); 
        assertTrue(eventManager.isTask2Done());
	}
}
