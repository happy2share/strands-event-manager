package com.strands.interviews.eventsystem;

public class Task1SimpleEventClassListenerMock implements InterviewEventListener {

	Class[] classes;
	public boolean subEventInvokedMe;
	
	public Task1SimpleEventClassListenerMock(Class[] classes) {
	        this.classes = classes;
	}

	@Override
	public void handleEvent(InterviewEvent event) {
		// TODO Auto-generated method stub
		System.out.println("SimpleEvent class InterviewEvent called now");
		subEventInvokedMe = true;
	}
	
	public boolean isCurrentListenerInvoked(){
		return subEventInvokedMe;
	}

	@Override
	public Class[] getHandledEventClasses() {
		// TODO Auto-generated method stub
		System.out.println( "Class called is " + classes[0].getSimpleName());
		return classes;
	}
}
