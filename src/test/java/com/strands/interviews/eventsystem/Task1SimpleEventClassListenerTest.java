package com.strands.interviews.eventsystem;

import org.junit.Test;

import com.strands.interviews.eventsystem.events.SimpleEvent;
import com.strands.interviews.eventsystem.events.SubEvent;
import com.strands.interviews.eventsystem.impl.DefaultEventManager;

import static org.junit.Assert.*;

public class Task1SimpleEventClassListenerTest {

	private EventManager eventManager = new DefaultEventManager();
	@Test
	public void testIsSimpleEventClassListening() {
		Task1SimpleEventClassListenerMock eventListenerMock = new Task1SimpleEventClassListenerMock(new Class[]{SubEvent.class});
        eventManager.registerListener("task1.key", eventListenerMock);
        eventManager.publishEvent(new SimpleEvent(this));
        assertTrue(eventListenerMock.isCurrentListenerInvoked());
	}
}
