Task 1:

Task 1 is acheived through the below classes

1) Task1SimpleEventClassListenerMock.java
2) Task1SimpleEventClassListenerTest.java

First class listens the event raised by second class. The 'handleEvent' is used for changing the status of invocation of SimpleEvent class by SubEvent class.

Task 2:

The following classes are added for Task 2

1) Task2EventListener.java
2) Task2EventListenerMock.java
3) Task2EventListenerTest.java

The DefaultEventManager and respective implemented interface EventManager has been modified to acheive Task 2. A new event listener called Task2EventListener is added for this task. DefaultEventManager is modified to invoke Task2EventListener in new test class Task2EventListenerTest. A new method is added to DefaultEventManager class named registerTask2Listener to check the emptyness of listening classes array and invoke all the event classes CreateEvent, SimpleEvent and SubEvent.
